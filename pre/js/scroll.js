$(document).ready(function() {
  $('#back-to-top').hide();
    var offset = 250;
    var duration = 500;

    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('#back-to-top').fadeIn(duration);
        } else {
            $('#back-to-top').fadeOut(duration);
        }
    });

    $('#back-to-top').click(function() {
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    });

    $("#submit").mouseleave(function(){

      $("#submit").fadeOut("fast", 1);
    });

    $("#submit").mouseenter(function(){

      $("#submit").fadeTo("fast", 0.9 );
    });
});
